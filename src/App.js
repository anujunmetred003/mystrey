import React, { Component } from 'react';
import {
  BrowserRouter as Router,
  Route,
  Switch,
} from 'react-router-dom';
import Headersec from './components/header.js';
import Home from './components/home.js';
import Dest from './components/destination.js';
import Inrtour from './components/tourlnk.js';
import Aboutctnt from './components/about.js';
import Hotelctnt from './components/hotel.js';
import contactctnt from './components/contact.js';
import Cabctnt from './components/cab.js';
import Fotersec from './components/footer.js';
import Innovacab from './components/innova.js';
import Aultocab from './components/aulto.js';
import Swiftcab from './components/swift.js';
import Kullu from './components/kullu.js';
import Shimla from './components/shimla.js';
import Dharamshala from './components/dharamshala.js';
import Hotelname from './components/singlehotel.js';
import Tourname from './components/singletour.js';
import { Link } from 'react-bootstrap/Navbar';


class App extends Component {

  render() {
    return (
    <Router>
      <div className="App">
        <Headersec />
        <Switch>
          <Route path="/" component={Home} exact />
          <Route path="/destination" component={Dest} />
          <Route path="/tour" component={Inrtour} />
          <Route path="/about" component={Aboutctnt} />
          <Route path="/hotel" component={Hotelctnt} />
          <Route path="/contact" component={contactctnt} />
          <Route path="/cab" component={Cabctnt} />
          <Route path="/Innova" component={Innovacab} />
          <Route path="/aulto" component={Aultocab} />
          <Route path="/swift" component={Swiftcab} />
          <Route path="/kullu" component={Kullu} />
          <Route path="/shimla" component={Shimla} />
          <Route path="/dharamshala" component={Dharamshala} />
          <Route path="/singlehotel" component={Hotelname} />
          <Route path="/singletour" component={Tourname} />
          
          </Switch>
          <Fotersec />
      </div>
    </Router>
    );
  }
}

export default App;
