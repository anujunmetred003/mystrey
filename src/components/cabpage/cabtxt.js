import React, { Component } from "react";
import Data from '../data/images.json'


const TITLE = "Let's find your perfect cab"


    class Cabpagesec extends Component {
        constructor(){
            super ();
            this.state = { displayBio: false };
            
        }
      toggleDisplayBio = () => {
          this.setState({ displayBio: !this.state.displayBio });
      }

render(){
    return(

<div>

<div className="inner-cab-banner">
          <div className="container text-center">
              <h1 className="inner-heading">{ TITLE }</h1>
          </div>
</div>

<div className="hotel-pge-sec">
    <div className="container">
        <div className="row">
        {Data.cabdata.map((cabtext, i) => (
            <div className="col-md-3 col-12 px-3 mb-3">
     <div className="cab-bar">
     <img src={cabtext.cabimage} class="img-fluid" />
               <div class="cab">
                  <h4>{cabtext.cabname}</h4>
                  <p>{cabtext.cabprice}</p> 
                  <a href={cabtext.caburl} class="btn">BOOK NOW</a>
               </div>
    
      </div>
            </div>
             ))}
        </div>
    </div>
</div>
{

this.state.displayBio?(

<div className="hotel-pge-sec">
<div className="container">
<div className="row">
{Data.cabloaddata.map((cabtext, i) => (
            <div className="col-md-3 col-12 px-3 mb-3">
     <div className="cab-bar">
     <img src={cabtext.cabimage} class="img-fluid" />
               <div class="cab">
                  <h4>{cabtext.cabname}</h4>
                  <p>{cabtext.cabprice}</p> 
                  <a href={cabtext.caburl} class="btn">BOOK NOW</a>
               </div>
    
      </div>
            </div>
             ))}
     <div className="lod_btn text-center w-100">
    <button type="button" onClick={this.toggleDisplayBio} className="btn load_btn">LOAD LESS CABS</button>
    </div>
</div>
</div>
</div>
): (
  <div className="lod_btn text-center">
  <button type="button" onClick={this.toggleDisplayBio} className="btn load_btn">LOAD MORE CABS</button>
  </div>


)
}

</div>
        
    )
    }

    }

export default Cabpagesec;