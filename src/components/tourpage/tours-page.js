import React from "react";
import OwlCarousel from 'react-owl-carousel';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';
import Data from '../data/images.json'


const TITLE = 'Our Tours'

const options = {
   margin: 10,
   responsiveClass: true,
   nav: true,
   dots: false,
   autoplay: false,
   smartSpeed: 1000,
   responsive: {
       0: {
           items: 1,
       },
       400: {
           items: 1,
       },
       600: {
           items: 2,
       },
       700: {
           items: 3,
       },
       1000: {
           items: 4,
 
       }
   },
 };



   const Tourpagesec = () => (

    <div>

<div className="inner-banner">
          <div className="container text-center">
              <h1 className="inner-heading">{ TITLE }</h1>
          </div>
</div>

      <div className="single-tour">
         <div className="container-fluid">
               <div className="col-12 position-relative mb-4">
                  <nav>
                     <div className="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
                        <a className="nav-item nav-link active tb-link" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">SHIMLA</a>
                     </div>
                  </nav>
                  <div className="tab-content py-3 px-3 px-sm-0" id="nav-tabContent">
                     <div className="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                     <OwlCarousel 
                     className="owl-theme"
                     {...options}
                     >
                     {Data.shimladetail.map((singletour, i) => (
                     
                     <div className="item brdr">
                        <img src={singletour.alltourimg} className="tour-img" />
                        <div className="tour-cntnt">
                           <h5 className="tours-heading">{singletour.alltourtitle}</h5>
                           <p className="tour-pera">{singletour.shimlalongdiscr}</p>
                           <div className="tour-pg-btn">
                            <a href={singletour.shimlatoururl} className="btn tour-bok-btn">BOOK NOW</a>
                           </div>
                        </div>
                     </div>
                    
                         ))}
                           </OwlCarousel>
                         
                     </div>
                  </div>
               </div>

               <div className="col-12 position-relative mb-4">
                  <nav>
                     <div className="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
                     <a className="nav-item nav-link active tb-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">MANALI</a>
                     </div>
                  </nav>
                  <div className="tab-content py-3 px-3 px-sm-0" id="nav-tabContent">
                     <div className="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                     <OwlCarousel 
                     className="owl-theme"
                     {...options}
                     >
                     {Data.manalidetail.map((manalitour, i) => (
                     
                     <div className="item brdr">
                        <img src={manalitour.alltourimg} className="tour-img" />
                        <div className="tour-cntnt">
                           <h5 className="tours-heading">{manalitour.alltourtitle}</h5>
                           <p className="tour-pera">{manalitour.shimlalongdiscr}</p>
                           <div className="tour-pg-btn">
                            <a href={manalitour.shimlatoururl} className="btn tour-bok-btn">BOOK NOW</a>
                           </div>
                        </div>
                     </div>
                    
                         ))}
                           </OwlCarousel>
                         
                     </div>
                  </div>
               </div>




               <div className="col-12 position-relative mb-4">
                  <nav>
                     <div className="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
                     <a className="nav-item nav-link active tb-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">DHARAMSHALA</a>
                     </div>
                  </nav>
                  <div className="tab-content py-3 px-3 px-sm-0" id="nav-tabContent">
                     <div className="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                     <OwlCarousel 
                     className="owl-theme"
                     {...options}
                     >
                     {Data.dharamshaladetail.map((dahramshalatour, i) => (
                     
                     <div className="item brdr">
                        <img src={dahramshalatour.alltourimg} className="tour-img" />
                        <div className="tour-cntnt">
                           <h5 className="tours-heading">{dahramshalatour.alltourtitle}</h5>
                           <p className="tour-pera">{dahramshalatour.shimlalongdiscr}</p>
                           <div className="tour-pg-btn">
                            <a href={dahramshalatour.shimlatoururl} className="btn tour-bok-btn">BOOK NOW</a>
                           </div>
                        </div>
                     </div>
                    
                         ))}
                           </OwlCarousel>
                         
                     </div>
                  </div>
               </div>



               <div className="col-12 position-relative mb-4">
                  <nav>
                     <div className="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
                     <a className="nav-item nav-link active tb-link" id="nav-about-tab" data-toggle="tab" href="#nav-about" role="tab" aria-controls="nav-about" aria-selected="false">PALAMPUR</a>
                     </div>
                  </nav>
                  <div className="tab-content py-3 px-3 px-sm-0" id="nav-tabContent">
                     <div className="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                     <OwlCarousel 
                     className="owl-theme"
                     {...options}
                     >
                     {Data.palampurdetail.map((palampurtour, i) => (
                     
                     <div className="item brdr">
                        <img src={palampurtour.alltourimg} className="tour-img" />
                        <div className="tour-cntnt">
                           <h5 className="tours-heading">{palampurtour.alltourtitle}</h5>
                           <p className="tour-pera">{palampurtour.shimlalongdiscr}</p>
                           <div className="tour-pg-btn">
                            <a href={palampurtour.shimlatoururl} className="btn tour-bok-btn">BOOK NOW</a>
                           </div>
                        </div>
                     </div>
                    
                         ))}
                           </OwlCarousel>
                         
                     </div>
                  </div>
               </div>


         </div>
      </div>
      </div>
        );
        
export default Tourpagesec;