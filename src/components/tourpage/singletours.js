import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Data from '../data/images.json';


class Singlehotel extends Component{

    render(){
        console.log('this,props', this.props);

        const { hoteltitle, hotelimg, hoteldiscription } = this.props.hotel; 

        return(
        <div style={{ display: 'inline-block', width: 300, margin: 10, textAlign: 'center' }}>
            <h3>{hoteltitle}</h3>
            <img src={hotelimg} className="img-fluid" />
        <p>{hoteldiscription}</p>
        </div>
        )
    }
}


export default Singlehotel;