import React from "react";
import axios from "axios";
import Data from '../data/images.json'


const TITLE = 'CONTACT US'

    class Contactpagesec extends React.Component {

    constructor(props){
        super(props);
         this.state = {
             firstname: '',
             lastname: '',
             email: '',
             phone: '',
             enquiry: '',
             message: ''
         }   
    }

    handleSubmit(e){
        e.preventDefault();
        axios({
          method: "POST", 
          url:"http://localhost:3000/send", 
          data:  this.state
        }).then((response)=>{
          if (response.data.status === 'success'){
            alert("Message Sent."); 
            this.resetForm()
          }else if(response.data.status === 'fail'){
            alert("Message failed to send.")
          }
        })
      }

      resetForm(){
    
        this.setState({firstname: '', lastname: '', email: '', phone: '', enquiry: '', message: '' })
     }

render(){
    return(
<div>
<div className="inner-banner">
          <div className="container text-center">
              <h1 className="inner-heading">{ TITLE }</h1>
          </div>
      </div>
<div className="contact-sec">
    <div className="container mb-3">
        <div className="row">
        {Data.contactdata.map((contactdata, i) => (
            <div className="col-md-4 col-12">
                <div className="contact-txt">
              <p>{contactdata.contactpera}</p>
              <a href={contactdata.contatclnk}><h3>{contactdata.contactheading}<br />{contactdata.contactaddrs}</h3></a>
                </div>
            </div>
     
              ))}
        </div>
    </div>
    <div className="contact-field-sec">
        <div className="container">
            <div className="row">
                <div className="col-md-6 col-12">
                    <div className="contact-bar">
                        <p>All*Marked fiels are compulsory</p>
                <form method="POST" onSubmit={this.handleSubmit.bind(this)} class="form-inlin">
                    <div className="col-12">
                            <div className="row">
                        <div className="col-md-6 col-12 px-0">                     
                              <div class="form-group">
                                <input type="text" id="firstname"  value={this.state.firstname} onChange={this.onNameChange.bind(this)} class="form-control name-fields" placeholder="First name*" required />
                              </div>
                        </div>
                        <div className="col-md-6 col-12 px-0">
                              <div class="form-group">
                                <input type="text" id="lastname"  value={this.state.lastname} onChange={this.onLastnameChange.bind(this)} class="form-control lastname-fields" placeholder="Last name*" required />
                              </div>
                            </div>
                        </div>
                      </div>
                    <div class="form-group">
                        <input type="text" id="email"  aria-describedby="emailHelp" value={this.state.email} onChange={this.onEmailChange.bind(this)} class="form-control field" placeholder="Email" required />
                    </div>
                    <div class="form-group">
                        <input type="text" id="phone" value={this.state.phone} onChange={this.onPhoneChange.bind(this)} class="form-control field" placeholder="Phone no.*" required />
                    </div>
                    <div class="form-group">
                        <input type="text" id="enquiry" value={this.state.enquiry} onChange={this.onEnquiryChange.bind(this)} class="form-control field" placeholder="Enquiry*" required />
                    </div>
                    <div class="form-group">
                        <textarea id="message" value={this.state.message} onChange={this.onMessageChange.bind(this)} row="5" class="form-control msg-field" placeholder="Message*"></textarea>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="message-btn btn-block btn-lg">SUBMIT</button>
                    </div>
                  </form>
                    </div>
                </div>
                <div className="col-md-6 col-12">
                    <div className="map-sec">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d14183.071183125914!2d77.15083413474676!3d31.093491708720393!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390578e3e35d6e67%3A0x1f7e7ff6ff9f54b7!2sShimla%2C%20Himachal%20Pradesh!5e0!3m2!1sen!2sin!4v1584085668935!5m2!1sen!2sin" width="820" height="560" frameborder="0" allowfullscreen="" aria-hidden="false" tabindex="0" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
);
}

onNameChange(event) {
	this.setState({firstname: event.target.value})
  }
  
  onLastnameChange(event) {
	this.setState({lastname: event.target.value})
  }

  onEmailChange(event) {
	this.setState({email: event.target.value})
  }

  onPhoneChange(event) {
	this.setState({phone: event.target.value})
  }

  onEnquiryChange(event) {
	this.setState({enquiry: event.target.value})
  }

  onMessageChange(event) {
	this.setState({message: event.target.value})
  }

        
            }   

export default Contactpagesec;