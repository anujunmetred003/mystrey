import React, { Component } from "react";
import Data from '../data/images.json'


const TITLE = 'HOTELS'


    class Hotelpagesec extends Component {
    constructor(){
        super ();
        this.state = { displayBio: false };
        
    }
  toggleDisplayBio = () => {
      this.setState({ displayBio: !this.state.displayBio });
  }


render(){
    return(


<div>

<div className="inner-banner">
          <div className="container text-center">
              <h1 className="inner-heading">{ TITLE }</h1>
          </div>
</div>

<div className="hotel-pge-sec">
    <div className="container">
        <div className="row">
        {Data.hoteldata.map((hotelsdata, i) => (
            <div className="col-md-3 col-12 px-1 mb-3">
     <div className="hotel-bar">
                  <img src={hotelsdata.hotelimg} className="img-fluid" />
                <div className="htl-cntnt">
                     <h3 className="htl-heading">{hotelsdata.hoteltitle}</h3>
                     <p className="htl-pera">{hotelsdata.hoteldiscription}</p>
                     <a href={hotelsdata.hotelurl} className="btn ex-btn">EXPLORE</a>
               </div>
      </div>
            </div>
             ))}
        </div>
    </div>
</div>

{

this.state.displayBio?(

<div className="hotel-pge-sec">
<div className="container">
<div className="row">
{Data.hotelloaddata.map((hotelsdata, i) => (
            <div className="col-md-3 col-12 px-1 mb-3">
     <div className="hotel-bar">
                  <img src={hotelsdata.hotelimg} className="img-fluid" />
                <div className="htl-cntnt">
                     <h3 className="htl-heading">{hotelsdata.hoteltitle}</h3>
                     <p className="htl-pera">{hotelsdata.hoteldiscription}</p>
                     <a href={hotelsdata.hotelurl} className="btn ex-btn" target="_blank">EXPLORE</a>
               </div>
      </div>
            </div>
             ))}
     <div className="lod_btn text-center w-100">
    <button type="button" onClick={this.toggleDisplayBio} className="btn load_btn">LOAD LESS HOTELS</button>
    </div>
</div>
</div>
</div>
): (
  <div className="lod_btn text-center">
  <button type="button" onClick={this.toggleDisplayBio} className="btn load_btn">LOAD MORE HOTELS</button>
  </div>


)
}





</div>
        
)
}
}

export default Hotelpagesec;