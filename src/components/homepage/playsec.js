import React from "react";
import Data from '../data/images.json';

  const Frthsec = () => (
    <div>
      {Data.advendata.map((advendetail, index) => (
      <div className="container position-relative play-sec">
         <img src={advendetail.raftingimg} className="raft-img" />
         <div className="play-icon-bar text-center">
            <a href={advendetail.playurl} className="play-link"><img src={advendetail.playimg} className="play-img" /></a>
         </div>
      </div>
          ))}
    </div>
  );
export default Frthsec;