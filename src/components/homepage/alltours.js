import React from "react";
import OwlCarousel from 'react-owl-carousel';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import 'react-tabs/style/react-tabs.css';
import Data from '../data/images.json'; 

const options = {
   margin: 10,
   responsiveClass: true,
   nav: true,
   dots: false,
   autoplay: false,
   smartSpeed: 1000,
   responsive: {
       0: {
           items: 1,
       },
       400: {
           items: 1,
       },
       600: {
           items: 2,
       },
       700: {
           items: 3,
       },
       1000: {
           items: 4,
 
       }
   },
 };



   const Alltoursec = () => (

    <div>
      <div className="tours-sec" id="tour-bar">
         <div className="container text-center">
            <h5 className="tour-heading">Find your tour from</h5>
            <h2 className="tour-dn-heading">OUR TOURS</h2>
            <div className="row">
               <div className="col-12 tabs-bar">
               <Tabs>
                     <TabList>
                     {Data.alltourdata.map((tourtab, index) => (
                        <Tab>{tourtab.destname}</Tab>
                      ))}    
                     </TabList>

                     <TabPanel>                     
                     <OwlCarousel 
                     className="owl-theme hotel-slider tab-slide" id="tab-slider"
                     {...options}
                     >
                     {Data.shimladetail.map((shimlatour, i) => (
                     
                           <div className="item no-brdr">
                              <img src={shimlatour.alltourimg} className="tour-img" />
                              <div className="tour-cntnt">
                                 <h5 className="tours-heading">{shimlatour.alltourtitle}</h5>
                                 <p className="tour-pera">{shimlatour.alltourdiscription}</p>
                              </div>
                           </div>
                          
                               ))}
                           </OwlCarousel>
                           </TabPanel>

                           <TabPanel>                     
                           <OwlCarousel 
                     className="owl-theme hotel-slider tab-slide" id="tab-slider"
                     {...options}
                     >
                     {Data.manalidetail.map((manalitour, i) => (
                     
                           <div className="item no-brdr">
                              <img src={manalitour.alltourimg} className="tour-img" />
                              <div className="tour-cntnt">
                                 <h5 className="tours-heading">{manalitour.alltourtitle}</h5>
                                 <p className="tour-pera">{manalitour.alltourdiscription}</p>
                              </div>
                           </div>
                          
                               ))}
                           </OwlCarousel>
                           </TabPanel>

                           <TabPanel>                     
                           <OwlCarousel 
                     className="owl-theme hotel-slider tab-slide" id="tab-slider"
                     {...options}
                     >
                     {Data.dharamshaladetail.map((dahramshalatour, i) => (
                     
                           <div className="item no-brdr">
                              <img src={dahramshalatour.alltourimg} className="tour-img" />
                              <div className="tour-cntnt">
                                 <h5 className="tours-heading">{dahramshalatour.alltourtitle}</h5>
                                 <p className="tour-pera">{dahramshalatour.alltourdiscription}</p>
                              </div>
                           </div>
                          
                               ))}
                           </OwlCarousel>
                           </TabPanel>

                           <TabPanel>                     
                           <OwlCarousel 
                     className="owl-theme hotel-slider tab-slide" id="tab-slider"
                     {...options}
                     >
                     {Data.palampurdetail.map((palampurtour, i) => (
                     
                           <div className="item no-brdr">
                              <img src={palampurtour.alltourimg} className="tour-img" />
                              <div className="tour-cntnt">
                                 <h5 className="tours-heading">{palampurtour.alltourtitle}</h5>
                                 <p className="tour-pera">{palampurtour.alltourdiscription}</p>
                              </div>
                           </div>
                          
                               ))}
                           </OwlCarousel>
                           </TabPanel>
                  </Tabs>
               </div>
            </div>
         </div>
      </div>
      </div>
        );
        
export default Alltoursec;