import React from "react";
import OwlCarousel from 'react-owl-carousel';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';
import Data from '../data/images.json';

const options = {
  margin: 10,
  responsiveClass: true,
  nav: true,
  dots: false,
  autoplay: false,
  smartSpeed: 1000,
  responsive: {
      0: {
          items: 1,
      },
      400: {
          items: 1,
      },
      600: {
          items: 2,
      },
      700: {
          items: 3,
      },
      1000: {
          items: 5,

      }
  },
};

  const Scndsec = () => (

    <div>
      <div className="hotel-sec" id="htl-sec">
         <div className="container text-center hotel-contain">
            <div className="hotel-heading-sec">
               <h2 className="hotel-heading">Hotels</h2>
               <p className="hotel-pera">Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic</p>
            </div>
      
      <OwlCarousel
    className="owl-theme hotel-slider"
    {...options}
>
        {Data.hoteldata.map((hotelsdata, i) => (
        <div key={i}>
          {" "}
     <div className="item">
                  <img src={hotelsdata.hotelimg} className="dest-img" />
                  <div className="htl-cntnt">
                     <h3 className="htl-heading">{hotelsdata.hoteltitle}</h3>
                     <p className="htl-pera">{hotelsdata.hoteldiscription}</p>
                     <a href={hotelsdata.hotelurl} className="btn ex-btn">EXPLORE</a>
               </div>
      </div>

      </div>
      ))}
</OwlCarousel>

          
      </div>
      </div>
    
     
    </div>

  );


export default Scndsec;





