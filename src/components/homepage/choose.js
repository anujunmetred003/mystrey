import React from "react";
import Data from '../data/images.json';

  const Chosesec = () => (

    <div>

      <div className="why-sec">
         <div className="container text-center">
            <h4 className="why-pera">Why Choose Us</h4>
            <h3 className="why-heading">MYSTIC MOUNTAINS</h3>
            <div className="row">
      
             {Data.chosedata.map((chosethedata, i) => (
               <div className="col-md-3 col-12">
                  <div className="feature-bar text-center">
                     <img src={chosethedata.choseimg} className="featr-img" />
                     <h5 className="featr-heading">{chosethedata.chosetitle}</h5>
                     <p className="featre-pera">{chosethedata.chosediscription}</p>
                  </div>
               </div>
          ))}
 
      </div>
      </div>
    </div>
     
    </div>

  );

export default Chosesec;





