import React from "react";
import AOS from 'aos';
import OwlCarousel from 'react-owl-carousel';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';
import Data from '../data/images.json';

const options = {
  margin: 10,
  responsiveClass: true,
  nav: true,
  dots: false,
  smartSpeed: 1000,
  responsive: {
      0: {
          items: 1,
      },
      400: {
          items: 1,
      },
      600: {
          items: 2,
      },
      700: {
          items: 3,
      },
      1000: {
          items: 5,

      }
  },
};

  const Frstsec = () => {

    AOS.init({
      duration: 2000
      });

      return(

    <div>
      <div className="tour-sec" data-aos="fade-up">
      <div className="container-fluid">
      
      <OwlCarousel
    className="owl-theme"
    {...options}
>
        {Data.tourowldata.map((touronedata, i) => (
        <div key={i}>
          {" "}
     <div className="item">
                  <img src={touronedata.tourimg} className="dest-img" />
                  <div className="dst-cntnt">
                     <h3 className="dst-heading">{touronedata.title}</h3>
                     <p className="dst-pera">{touronedata.discription}</p>
                     <p className="dest-dn-pera">{touronedata.type}</p>
               </div>
      </div>

      </div>
      ))}
</OwlCarousel>

          
      </div>
      </div>
    
     
    </div>
      )

        };


export default Frstsec;





