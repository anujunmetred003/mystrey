import React from "react";
import AOS from 'aos';
//import 'aos/dist/aos.css';
import Data from '../data/images.json';


   const Thrdsec = () => {

      AOS.init({
         duration: 2000
         });

      return(

    <div>
      {Data.advendata.map((advendetail, index) => (
         <div className="bnnr-img" data-aos="fade-up">
         <div className="container text-center">
            <div className="bnnr-inr">
      
               <h3 className="bnnr-heading">{advendetail.adventrhedng}</h3>
               <h1 className="bnnr-dn-heading">{advendetail.adventrdnhedng}</h1>
    
            </div>
         </div>
      </div>
          ))}
    </div>
      )
   };
export default Thrdsec;





