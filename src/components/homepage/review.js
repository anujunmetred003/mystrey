import React, {Component} from "react";
import Carousel from 'react-bootstrap/Carousel' 
import Data from '../data/images.json';

      export class Reviwesec extends Component {
         render(){
            return(

<div>
<div className="review-sec">
<div className="container text-center rew-sec">
   <div className="bd-example">
      <div id="carouselExampleCaptions" className="carousel slide" data-ride="carousel">
         <Carousel>
         
         {Data.reviewdata.map((reviews, i) => (
            <Carousel.Item>
     <h5 className="rew-heading">{reviews.reviewtitle}</h5>
     <h2 className="rew-dn-heading">{reviews.reviewsectitle}</h2>
     <img src={reviews.reviewimg} className="rew-img" alt="face-img" />
     <p className="rew-pera">{reviews.reviewdiscription}</p>
</Carousel.Item>
 ))}
         
         </ Carousel>
      </div>
   </div>
</div>
</div>
</div>
            )
         }
      }




export default Reviwesec;