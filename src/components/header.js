import React from "react";
import Data from './data/images.json';
import { Link } from 'react-router-dom';



   const Headersec = () => (

    <div>
      
          <div className="upr-contact">
         <div className="container upr-bar">
            <div className="row">
               <div className="col-6">
                  <ul className="social-lists">
                     <li className="web-list-item">
                     {Data.topbarconatact.map((topbar, index) => (
                     <a href={topbar.tellink} className="web-link"><img src={topbar.telimg} className="web-img fadeIn" /><span className="web-no  fadeIn">{topbar.telno}</span></a>
                     ))}
                     </li>
                  </ul>
               </div>
               <div className="col-6 social-bar">
                  <ul className="social-lists">
                     <li className="social-list-item">
                        <a href="#" className="social-list-link"><img src="../images/instagram.png" className="social-img fadeIn" /></a>
                        <a href="#" className="social-list-link"><img src="../images/twitter.png" className="social-img fadeIn" /></a>
                        <a href="#" className="social-list-link"><img src="../images/facebook.png" className="social-fb-img fadeIn" /></a>
                        <a href="#" className="social-list-link"><img src="../images/pintrest.png" className="social-img fadeIn" /></a>
                        <a href="#" className="social-list-link"><img src="../images/youtube.png" className="social-img fadeIn" /></a>
                     </li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
    <header id="main-header">
         <div className="container nav-bar">
            <nav className="navbar navbar-expand-lg navbar-light nav-bar nav-header">
               <div className="row w-100 resp">
                  <button className="navbar-toggler toggle collapsed" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                  <span className="navbar-toggler-icon"></span>
                  </button>
                  <div className="logo-resp col">
                  {Data.dsktoplogo.map((logodata, i) => (
                     <a href={logodata.logolink} className="logo-link"><img src={logodata.logoimg} className="logo-image" /></a>
                     ))}    
                  </div>
               </div>
               <div className="collapse navbar-collapse back-color" id="navbarSupportedContent">
                  <ul className="navbar-nav mr-auto left-nav">
                  {Data.leftmenu.map((menu, i) => (
                     <li className="nav-item menu-list">
                     <Link to={menu.menulink} className="nav-link menu">
                           <img src={menu.menuimg} className="menu-img bounceInUp" />
                           <p>{menu.menutxt}</p>
                       </Link>
                     </li>
                     ))}
                  </ul>
                  <div className="logo text-center">
                  {Data.dsktoplogo.map((logodata, i) => (
                     <a href={logodata.logolink} className="logo-link"><img src={logodata.logoimg} className="logo-image" /></a>
                     ))}
                  </div>
                  <ul className="navbar-nav right-nav ml-auto">
                  {Data.rightmenu.map((menu, i) => (
                     <li className="nav-item menu-list">
                     <Link to={menu.menulink} className="nav-link menu">
                           <img src={menu.menuimg} className="menu-img bounceInUp" />
                           <p>{menu.menutxt}</p>
                       </Link>
                     </li>
                     ))}
                  </ul>
               </div>
            </nav>
         </div>
      </header>
        </div>

  );

export default Headersec;