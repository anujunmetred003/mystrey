import React, { Component } from 'react';
import ReactDom from 'react-dom';
import Data from '../data/images.json';

const TITLE = 'DESTINATIONS'

//export default class Destitem extends  {

  class Destitem extends Component {
    constructor(){
        super ();
        this.state = { displayBio: false };
        
    }
  toggleDisplayBio = () => {
      this.setState({ displayBio: !this.state.displayBio });
  }



render(){
return (
  <div>
      <div className="inner-banner">
          <div className="container text-center">
              <h1 className="inner-heading">{ TITLE }</h1>
          </div>
      </div>

      <div className="more-destination">
        <div className="container destion-wdth">
      
    <div className="row">
      {Data.destinationdata.map((destdata, index) => (
        <div className="col-md-4 col-12 dest_sec mb-2">
        <a href={destdata.url} className="dest_link">
          <img src={destdata.destpageimg} className="img-fluid" />
        <div className="destinton-txt">
          <h3>{destdata.destpageimgtitle}</h3>
          <p>{destdata.destpageimgpera}</p>
          <p className="dest_item_vlg">{destdata.destpageimgsuit}</p>
        </div>
        </a>
        </div>
        
      ))}
</div>
</div>
</div>

{

  this.state.displayBio?(

<div className="loadsec">
  <div className="container destion-wdth">
<div className="row">
{Data.destinationloaddata.map((destdata, index) => (
        <div className="col-md-4 col-12 dest_sec mb-2">
        <a href={destdata.url} className="dest_link">
          <img src={destdata.destpageimg} className="img-fluid" />
        <div className="destinton-txt">
          <h3>{destdata.destpageimgtitle}</h3>
          <p>{destdata.destpageimgpera}</p>
          <p className="dest_item_vlg">{destdata.destpageimgsuit}</p>
        </div>
        </a>
        </div>
        
      ))}
       <div className="lod_btn text-center w-100">
      <button type="button" onClick={this.toggleDisplayBio} className="btn load_btn">LOAD LESS DESTINATIONS</button>
      </div>
</div>
</div>
</div>
  ): (
    <div className="lod_btn text-center">
    <button type="button" onClick={this.toggleDisplayBio} className="btn load_btn">LOAD MORE DESTINATIONS</button>
    </div>


)
}
</div>

)
    }
  }

export default Destitem;