import React from "react";
import Data from '../data/images.json'


const TITLE = 'ABOUT US'


const Aboutpagesec = () => (

<div>
<div className="inner-banner">
          <div className="container text-center">
              <h1 className="inner-heading">{ TITLE }</h1>
          </div>
      </div>

      <div className="why-sec mt-5">
         <div className="container text-center">
            <h4 className="why-pera">Why Choose Us</h4>
            <h3 className="why-heading">MYSTIC MOUNTAINS</h3>
            <div className="row">
      
             {Data.chosedata.map((chosethedata, index) => (
               <div className="col-md-3 col-12">
                  <div className="feature-bar text-center">
                     <img src={chosethedata.choseimg} className="featr-img" />
                     <h5 className="featr-heading">{chosethedata.chosetitle}</h5>
                     <p className="featre-pera">{chosethedata.chosediscription}</p>
                  </div>
               </div>
          ))}
 
      </div>

      <div className="about-bar">
      
          <div className="row">
              <div className="col-md-6 col-12"> 
              {Data.aboutdata.map((chosethedata, index) => (
              <div className="about-pera">
          <p>{chosethedata.aboutpra}</p>
              </div>
              ))}
              </div>
               
              <div className="col-md-6 col-12">
              {Data.aboutdata.map((chosethedata, index) => (
               <div className="about-img">
                     <img src={chosethedata.aboutimage} className="img-fluid" /> 
              </div>
              ))}
              </div>

            
          </div>
        
           
      </div>
      </div>
    </div>



</div>

);

export default Aboutpagesec;